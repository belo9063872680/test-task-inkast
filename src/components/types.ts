export type TComment = {
  id: number;
  text: string;
  name: string;
  nickname: string;
  verified: boolean;
  date: string;
  likes?: number;
  avatar?: string;
};

export type TPost = {
  id: number;
  text: string;
  name: string;
  nickname: string;
  verified: boolean;
  date: string;
  likes?: number;
  comments?: TComment[];
  reposts: number;
  avatar?: string;
};
